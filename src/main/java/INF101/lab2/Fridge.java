package INF101.lab2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    // Array som har FridgeItem som felt variabler
    int maxItems = 20;

    ArrayList<FridgeItem> fridgeItems;
    public Fridge() {
        this.fridgeItems = new ArrayList<>();
    }
    @Override
    public int nItemsInFridge() {
        return fridgeItems.size();
    }
    @Override
    public int totalSize() {
        return maxItems;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (item == null) {
            throw new IllegalArgumentException();
        } else if (fridgeItems.size() >= totalSize()) {
            return false;
        }
        return fridgeItems.add(item);
    }


    @Override
    public void takeOut(FridgeItem item) {
        if ( !fridgeItems.contains(item)){
            throw new NoSuchElementException("This element does not exist");
        }
        else{
            fridgeItems.remove(item);
        }



    }
    // Remove all items from the fridge
    @Override
    public void emptyFridge() {
    fridgeItems.clear();
    }

    //	 * Remove all items that have expired from the fridge
    //	 * @return a list of all expired items
    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> hasExpired = new ArrayList<>();
        for(FridgeItem item : fridgeItems){
            if (item.hasExpired()){
                hasExpired.add(item);
            }
        }
        fridgeItems.removeAll(hasExpired);

        return hasExpired;
    }
}


